# Virtuelle Umgebungen <a title="Sourcecode auf bitbucket.org ansehen" href="https://bitbucket.org/fsimonjetz/ling-tools/raw/master/docs/venv.md"><img src="../img/bitbucket-icon.png" alt="Bitbucket icon"/></a>


## Allgemein

So praktisch und (meistens) einfach die Installation von
zusätzlichen Python-Bibliotheken per `pip` ist, gibt es doch
einen großen Nachteil: die jeweiligen Bibliotheken werden
standardmäßig global für das ganze System installiert. Das
kann zu Problemen führen - wenn Sie z.B. Ihr Projekt
aus diesem Kurs in einem Jahr noch mal benutzen möchten,
aber zwischenzeitlich Ihre SpaCy-Installation aktualisiert
haben, ist nicht garantiert, dass dieselben Ergebnisse
herauskommen. Schlimmstenfalls funktioniert es gar nicht mehr
und Sie müssen Ihren Code mühsam aktualisieren. Auch, wenn
mehrere Personen an demselben Projekt arbeiten, kann es
aufgrund verschiedener Python- und Bibliotheksversionen zu
unerwarteten Ergebnissen kommen. In solchen Fällen lohnt
sich die Einrichtung einer **virtuellen Umgebung**,
einer lokal abgekapselten Python-Installation, in
der Sie separate Versionen der jeweils benötigten Bibliotheken
installieren können, unabhängig von der globalen Installation
Ihres Systems.

## Einrichtung

### Option A: venv
In aktuellen Python-Versionen ist das `venv`-Modul bereits
vorinstalliert. Zur Erzeugung einer neuen virtuellen Umgebung
mit dem Namen *chatbot_venv* muss der folgende Befehl
ausgeführt werden:

```bash
python3 -m venv pfad/zu/chatbot_venv
```

Dies erzeugt einen neuen Ordner mit diversen Dateien:

```
chatbot_venv/
├── bin
│   ├── Activate.ps1
│   ├── activate
│   ├── activate.csh
│   ├── activate.fish
│   ├── easy_install
│   ├── easy_install-3.9
│   ├── pip
│   ├── pip3
│   ├── pip3.9
│   ├── python -> python3
│   ├── python3 -> /usr/local/bin/python3
│   └── python3.9 -> python3
├── include
├── lib
│   └── python3.9
└── pyvenv.cfg
```

Um die virtuelle Umgebung zu aktivieren, müssen Sie folgenden
Befehl ausführen (vorausgesetzt Sie nutzen bash):

```bash
source pfad/zu/chatbot_venv/bin/activate
```

Danach sollte sich der Prompt der Kommandozeile ändern und
die Befehle `which python` und `which pip` sollten auf
Binarys in der virtuellen Umgebung zeigen. Bei mir sieht das
bspw. so aus:

```
Fabians-MacBook-Pro:chatbot_venv simonjetz$ source ./bin/activate
(chatbot_venv) Fabians-MacBook-Pro:chatbot_venv simonjetz$ which python
/Users/simonjetz/projects/chatbot_venv/bin/python
(chatbot_venv) Fabians-MacBook-Pro:chatbot_venv simonjetz$ which pip
/Users/simonjetz/projects/chatbot_venv/bin/pip
```

Es wird also eine lokale Kopie von der System-Installation von Python erstellt
und der `source`-Befehl sagt dem Terminal, dass es diese Version nutzen soll.
Folglich können Ihre sonstigen Projektdaten auch woanders liegen; es
macht aber schon Sinn, alles zusammen in einem Ordner zu haben, z.B. so:

```
chatbot/
├── chatbot_venv
├── docs
│   └── [Dokumentation]
└── src
    └── [Code]
```

Zum Verlassen der Umgebung können Sie einfach das Fenster schließen oder
den Befehl `deactivate` eingeben.

### Option B: pipenv

Eine weitere Alternative, die m.E. gut funktioniert, ist
[pipenv](https://pypi.org/project/pipenv/). Nach der
Installation per `pip install pipenv` läuft die Erzeugung einer virtuellen
Umgebung ähnlich wie mit venv:

```bash
mkdir pfad/zu/chatbot_venv/ # Projektordner erzeugen
cd pfad/zu/chatbot_venv/    # in Projektordner wechseln
pipenv --three              # neue Umgebung erzeugen
```

Wenn Sie noch nicht viel mit virtuellen Umgebungen gearbeitet haben, *kann*
pipenv einfacher zu benutzen sein, da im Projektordner selbst nur wenige
Dateien angelegt werden und die Bedienung etwas intuitiver wirkt;
anstelle des `source`-Befehls wird die virtuelle Umgebung per `pipenv shell`
aus dem Projektordner heraus aktiviert und mit `exit` beendet. Wenn Sie
lediglich `pipenv` angeben, bekommen Sie außerdem eine recht gute Übersicht
über die Befehle. Man muss sich also, wie ich finde, nicht so viel merken.

Eigentlich ist es nun so gedacht, dass man statt `pip` alles per `pipenv`
verwaltet, aber man kann auch ganz traditionell mit `pip` arbeiten (s.u.).

## Arbeiten in der virtuellen Umgebung

Nach Aktivierung können Bibliotheken innerhalb der virtuellen Umgebung ganz
normal mit `pip` installiert werden und Sie arbeiten wie immer an Ihrem Code.
Um Projekte zu teilen, können alle installierten Bibliotheken mithilfe des
Befehls `pip freeze` angezeigt werden:

```
$ pip freeze
beautifulsoup4==4.9.3
Flask==1.1.2
Flask-SocketIO==4.3.1
Flask-WTF==0.14.3
mkdocs==1.1.2
```

Um diese Voraussetzungen mit anderen zu teilen, können Sie sie direkt
in eine Textdatei schreiben lassen:

```
$ pip freeze >> requirements.txt
```

Die Empfänger können dann ebenfalls eine virtuelle Umgebung anlegen und per
`pip install -r requirements.txt` mit einem Schlag sämtliche Bibliotheken
installieren, die für das Projekt benötigt werden.
