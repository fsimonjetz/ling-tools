# Python (nicht nur) für Fortgeschrittene <a title="Sourcecode auf bitbucket.org ansehen" href="https://bitbucket.org/fsimonjetz/ling-tools/raw/master/docs/tips.md"><img src="../img/bitbucket-icon.png" alt="Bitbucket icon"/></a>

An dieser Stelle werden hilfreiche Python-Bibliotheken
und -techniken gesammelt.


## List comprehensions

Wie Sie wissen, kann man in Python mit `[]` eine leere Liste erzeugen:

```python
my_list = []
```

Diese Liste kann jedoch auch direkt mit Inhalt befüllt werden, und zwar
mittels einer `for`-Schleife. Das ist praktisch, wenn man Dinge filtern
möchte, z.B. eine Liste aus `("token", "pos")`-Tupeln zu einer Liste von
Tokens:

```python
data = [("Das", "DT"),
        ("ist", "VV"),
        ("ein", "DT"),
        ("Beispiel", "NN"),
        (".", "$.")]

# Ergebnis: ["Das", "ist", "ein", "Beispiel", "."]
tokens = [w[0] for w in data]
```

Die letzte Zeile macht dabei genau dasselbe wie das:

```python
tokens = []

for w in data:
    tokens.append(w[0])
```

Man kann das gleiche mit Dictionarys machen, wobei die Syntax etwas
anders aussieht; wollen wir z.B. die Liste `data` (s.o.) in ein Dictionary
umwandeln, können wir sowas machen:

```python
data_dict = {w[0]:w[1] for w in data}

# Ergebnis: {"Das":"DT", "ist":"VV", ...}
```

...was das gleiche macht wie...

```python
data_dict = {}

for w in data:
    data_dict[w[0]] = w[1]

```

## collections

Das [`collections`-Modul](https://docs.python.org/3.8/library/collections.html)
enthält eine Reihe praktischer Funktionen, die einem den Programmieralltag
stark vereinfachen können.

### collections.Counter()

Wir müssen sehr häufig Dinge zählen, z.B. Wörter, wofür
wir idR ein Dictionary der Form `{"Element":Anzahl}` aufbauen.
Viel einfacher geht es mit `Counter()`.

```python
from collections import Counter

words = ["Das", "ist", "eine", "Liste", "mit", "vielen",
         "vielen", "vielen", "Wörtern", ".", ".", "."]

# Manuelle Methode
wcount = dict()

for w in words:
    if w in wcount:
        wcount[w] += 1
    else:
        wcount[w] = 1

# Mit Counter()
wcount = Counter(words)

# Oder so
wcount = Counter()

for w in words:
    # [irgendwas mit w machen wie filtern o.ä.]
    wcount[w] += 1 # <-- man muss nicht checken, ob w schon vorhanden ist!

# Gibt die häufigsten 10 Elemente sortiert aus
print(wcount.most_common(10))
```

### collections.defaultdict()

Wenn wir ein Dictionary mit Listen, Sets o.ä., also Sequenzen als Werte haben,
z.B. `pos_map = {"NN":["Token1", "Token2"]}`, müssen wir oft erst prüfen, ob
das Element bereits im Dictionary enthalten ist, und den neuen Wert dann
entweder an die vorhandene Liste anhängen oder eine neue leere Liste einfügen:

```python
pos_map = {"NN":["Token1", "Token2"]}

# neues Element
token, pos = "Token3", "VV"

if pos in pos_map:
    pos_map[pos].append(token)
else:
    pos_map[pos] = [token]
```

`defaultdict` vereinfacht das, indem es bei nicht vorhandenen Werten intern
eine neues Liste, ein neues Set etc. erzeugt. Dafür muss zunächst ein leeres
`defaultdict` erzeugt werden, wobei wir angeben müssen, was für einen Typ
die Werte haben sollen (meistens `list`, das kann aber alles mögliche sein).
Ansonsten ist das Verhalten identisch zu normalen Dictionarys.

```python
from collections import defaultdict

# Achtung: das Argument ist list, keine Anführungszeichen, keine Klammern!
pos_map = defaultdict(list)

pos_map["NN"] = ["Token1", "Token2"]

token, pos = "Token3", "VV"

# neues Element kann direkt hinzugefügt werden!
pos_map["VV"].append(token)
```


### collections.namedtuple()

Im Gegensatz zu normalen Tupeln haben die Elemente im `namedtuple`
Namen (*Thank you, Captain Obvious!*). Das kann ganz praktisch sein,
wenn man einzelne Elemente nicht per Index, sondern per Label abfragen
möchte, z.B. in HTML-Templates.

```python
from collections import namedtuple

Token = namedtuple("Token", ["text", "pos"])

my_token = Token("Wort1", "NN")

# Ausgabe: Token(text='Wort1', pos='NN')
print(my_token)

# Ausgabe in beiden Fällen: Wort1 NN
print(my_token[0], my_token[1])
print(my_token.text, my_token.pos)

```

## json

Für die Übertragung von Daten, z.B. über [APIs](apis.md), oder auch nur die
Speicherung, ist JSON eine verbreitete Möglichkeit. Für das Einlesen und
die Ausgabe von Daten in diesem Format gibt es das
[json-Modul](https://docs.python.org/3/library/json.html). Dabei werden auch
Sonderzeichen umgewandelt etc., es macht also durchaus mehr, als einfach
nur Datenstrukturen zu printen, auch wenn es auf den ersten Blick so aussieht.
Achtung: Mit manchen Datentypen kann das Modul nicht umgehen. Sets müssen bspw.
in Listen umgewandelt werden. Die vier wichtigsten Funktionen sind
`json.load()` und `json.loads()` zum einlesen, und `json.dump()` bzw.
`json.dumps()` zum speichern von Daten in JSON:

```python
import json

data = {"NN":["Token1", "Token2"]}

# Erzeugt JSON-String
json_data = json.dumps(data)

# Speichern in Datei
with open("data.json", mode="w") as jsonfile:
    json.dump(data, jsonfile)

# Einlesen eines JSON-Strings
json.loads("{'NN':['Token1', 'Token2']}")

# Einlesen einer JSON-Datei
with open("data.json", mode="r") as jsonfile:
    json.load(jsonfile)

```


## sys.argv

Um Programme aus der Kommandozeile aufzurufen und dabei Argumente zu
übergeben, können wir mit `sys.argv` eine Liste der Argumente bekommen,
die der Nutzer beim Programmaufruf übergeben hat.

```python
import sys

# Eine Liste der Kommandozeilen-Argumente, wobei das erste Element immer
# der Name des Moduls selbst ist
arguments = sys.argv
```

So kann man z.B. Hinweise zur Nutzung des Programms printen, wenn der
Nutzer "help" eingibt, oder Dateipfade abfragen oder oder oder...
Eine noch umfangreichere Funktionalität bietet das Modul [`argparse`](https://docs.python.org/3/library/argparse.html).
