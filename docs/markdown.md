# Markdown-Beispiele <a title="Sourcecode auf bitbucket.org ansehen" href="https://bitbucket.org/fsimonjetz/ling-tools/raw/master/docs/markdown.md"><img src="../img/bitbucket-icon.png" alt="Bitbucket icon"/></a>


## Überschriften
Mit einem oder mehreren Hashes `#` können Überschriften markiert werden -
parallel zu HTML-Überschriften:

# # H1
## ## H2
### ### H3
#### #### H4
##### ##### H5

## Textauszeichnung

Mit Sternchen * oder Underscores _ kann man
*&ast;kursiv&ast;* oder **&ast;&ast;fett&ast;&ast;** drucken, oder sogar
***&ast;&ast;&ast;beides&ast;&ast;&ast;***.

## Listen
1. Punkt Eins
2. Punkt Zwei
    - Unterlisten müssen in mkdocs...
    - ...mit vier Leerzeichen eingerückt werden.

Markdown ist an sich nur eine einfache Art und Weise, HTML zu schreiben. Wenn wir bspw. etwas
*&ast;kursiv&ast;* schreiben, macht ein Markdown-Editor
oder mkdocs daraus HTML der Form `<em>kursiv</em>`.
Komplexere Auszeichnungen kann man daher auch direkt mit HTML einbetten. Was man häufiger
braucht, ist bspw. der `</br>`-Tag, mit dem man einen Zeilenumbruch markieren kann.
Ohne so eine Anweisung markiert eine Leerzeile einen neuen Absatz, ansonsten
werden Zeilenumbrüche wie anderer Whitespace behandelt.

## Links und Bilder

Links zu Dateien oder Websites können mit der Syntax `[Linktext](url)` erzeugt werden.
[Hier](https://www.rub.de/) ist z.B. ein Link zur Uni.
Optinal kann man nach der URL einen Titel angeben, der angezeigt wird,
wenn man die Maus über den Link hält, z.B.
[so](https://www.linguistics.ruhr-uni-bochum.de/ "Linguistik-Website"),
bzw. im Quellcode:

```
[so](https://www.linguistics.ruhr-uni-bochum.de/ "Linguistik-Website")
```

Bilder können auf ähnliche Weise eingefügt werden, allerdings
mit einem vorangestellten Ausrufezeichen (also `![Bildbeschreibung](link/zum/bild.png)`). Änderungen
der Bildgröße usw. sind nicht direkt möglich; ggf. müssen
Bilder entweder mit einem externen Programm vorher skaliert
oder per HTML eingebunden werden (s.u.).

![Uni Logo](img/rub-logo.png)

Skaliertes Bild mit einem eingebetteten HTML Tag
(`<img src="img/rub-logo.png" alt="RUB" title="RUB Logo klein" style="width: 50px" />`):

<img src="../img/rub-logo.png" alt="RUB"
	title="RUB Logo klein" style="width: 50px" />

## Code

Inline-Code kann mit Backticks \`code\` hervorgehoben werden,
z.B. `import sys`. Code-Blocks werden mit drei Backticks
markiert. Wenn zu Beginn die Programmiersprache festgelegt
wird, wird der Code automatisch mit Syntax-Highlighting
dargestellt.

```python
from collections import Counter
from pprint import pprint
import sys


def word_stats(text):
    words = Counter(text.split())
    return words

if __name__ == "__main__":

    arguments = sys.argv

    with open(arguments[1], mode="r", encoding="utf-8") as infile:
        text = infile.read()

    pprint(word_stats(text))
```

## Mkdocs

Mithilfe der Bibliothek [mkdocs.org](https://www.mkdocs.org) kann man eine
vollständige Dokumentation in Markdown verfassen und daraus eine HTML-Webseite
generieren.

Das Modul muss mit `pip` installiert werden, z.B. so:

```bash
pip install mkdocs
```

Um ein neues Projekt zu erzeugen, öffnet man in der Kommandozeile den Pfad,
wo die Doku stehen soll, und führt folgenden Befehl aus, wobei `meine_doku`
ein beliebiger Name sein kann:

```bash
python -m mkdocs new meine_doku
```

Es wird eine Ordnerstruktur erzeugt, die aus einer Konfigurationsdatei
`mkdocs.yml` und einem `docs`-Ordner besteht. In letzterem speichern Sie Ihre
Markdown-Dokumente und andere Dateien. In der Konfigurationsdatei können Sie
bspw. das Theme der Doku ändern oder Add-ons hinzufügen. Für den Anfang soll
das Einfügen einer Navigationsleiste genügen, durch die die einzelnen Seiten
oben im Menü verlinkt werden. Die Datei hat dann folgenden Inhalt:

```yml
site_name: My Docs
nav:
  - Home: index.md
  - Markdown: markdown.md
```

Mit dem Befehl `python -m mkdocs build` kann die Dokumentation als statische
HTML-Seite generiert werden. Zum Schreiben und nur kurz anschauen ist aber der
Befehl `python -m mkdocs serve` viel praktischer: dieser erzeugt einen lokalen
Server, der die Seiten bei Änderungen automatisch aktualisiert (und bei
Problemen Warnungen und Errors ausgibt). Nach dem Ausführen sollte man die
Dokumentation im Browser unter http://localhost:8000 einsehen können. Somit
braucht man auch kein spezielles Programm, um den Markdown-Sourcecode
darzustellen - ein beliebiger Texteditor genügt. Dennoch ist es empfehlenswert,
einen guten Editor zum programmieren zu verwenden, wie z.B. den
[Atom Editor](https://atom.io/), der u.a. auch Markdown anzeigen kann.

## Weitere Infos

Weitere Hintergründe über Markdown, speziell über die von mkdocs
benutzte Variante, sind
[hier](https://daringfireball.net/projects/markdown/syntax)
schön zusammengefasst.
