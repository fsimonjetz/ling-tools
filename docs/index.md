# Entwicklung linguistischer Tools: Code-Dokumentation <a title="Sourcecode auf bitbucket.org ansehen" href="https://bitbucket.org/fsimonjetz/ling-tools/raw/master/docs/index.md"><img src="img/bitbucket-icon.png" alt="Bitbucket icon"/></a>

Hier werden Code-Beispiele aus dem Kurs gesammelt und dokumentiert. Per Klick
auf das Bitbucket-Symbol
<img src="img/bitbucket-icon.png" alt="drawing"/> im Titel des
jeweiligen Artikels gelangen Sie zum Sourcecode für die Seite.

## Themen

- [Markdown und mkdocs](markdown.md)
- [Modularisierung](modules.md)
- [Tipps und Tricks](tips.md)
- [Bots](bots.md)
- [APIs](apis.md)
- [Virtuelle Umgebungen](venv.md)
