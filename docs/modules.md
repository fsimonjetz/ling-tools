# Modularisierung <a title="Sourcecode auf bitbucket.org ansehen" href="https://bitbucket.org/fsimonjetz/ling-tools/raw/master/docs/modules.md"><img src="../img/bitbucket-icon.png" alt="Bitbucket icon"/></a>


Der Code für dieses Thema ist im
[Kurs-Repository](https://bitbucket.org/fsimonjetz/ling-tools/src/master/src/examples/modules/)
abgelegt.

Komplexere Programme sowie Funktionen, die man häufig braucht, sollte man nicht
immer wieder neu schreiben, kopieren o.ä., sondern in einer Python-Datei an
einem festen Ort ablegen und von dort aus importieren. So spart man sich auf
Dauer viel Zeit und Arbeit, kann sicher sein, dass die Funktion immer dasselbe
macht, und muss Aktualisierungen nur einmal machen.

Ein *Modul* ist eine `.py`-Datei, die eine oder mehrere Funktionen, Klassen,
Konstanten o.ä. enthält, die wir aus anderen Skripten oder Modulen heraus
importieren können. Eine einfache Dateistruktur kann z.B. so aussehen...

- Projektordner
    - count_words.py
    - linguistics.py

...mit folgendem Inhalt in `count_words.py`:

```python
# Inhalt von count_words.py
from collections import Counter
from pprint import pprint
import sys


def word_stats(text):
    '''Zaehlt die Woerter eines Textes'''

    words = Counter(text.split())
    return words


if __name__ == "__main__":

    arguments = sys.argv

    with open(arguments[1], mode="r", encoding="utf-8") as infile:
        text = infile.read()

    pprint(word_stats(text))
```

Die Datei `count_words.py` hat zwei Funktionen, und das ist der Grund,
warum wir den `if __name__ == "__main__":`-Block brauchen (s.u.).
Das Programm kann zunächst als eigenständiges Skript benutzt werden, um die
Wörter aus einer Text-Datei zu zählen. Der Aufruf lautet wie folgt, wobei
`textdatei.txt` eine beliebige Textdatei sein kann:

```
python count_words.py textdatei.txt
```

`sys.argv` enthält hierbei eine Liste der Kommandozeilen-Argumente (das erste
Element ist der Name der Datei selbst), und `Counter` ist eine Art
erweitertes Dictionary, das die Elemente aus der Wortliste zählt und in der
Form `{"Wortform":Anzahl}` speichert.

Zweitens fungiert die Datei als Modul, das die `word_stats()`-Funktion
für andere Python-Programme bereitstellt. Im Skript `linguistics.py` können
wir die Funktion aus `count_words.py` folgendermaßen importieren:

```python
# Inhalt von linguistics.py
from count_words import word_stats

my_text = """Das ist ein Text. Mit ganz vielen Wörtern!
             Das ist eigentlich ein schlechtes Beispiel,
             weil die meisten Wörter nur einmal vorkommen."""

print(word_stats(my_text))
```

Was soll nun die Anweisung `if __name__ == "__main__"`?
Hierzu muss man zunächst wissen, dass jedes Python-Skript, das wir schreiben,
einen Namen hat, der in der globalen Variable `__name__` gespeichert ist.
Wenn das Python-Modul, z.B. per Kommandozeile, *direkt* aufgerufen wird, ist
der interne Name des Moduls `"__main__"`; in diesem Fall wird alles, was
innerhalb des `if __name__ == "__main__":`-Blocks steht, tatsächlich
ausgeführt.

In dem Moment, wo wir im Modul `linguistics.py` den Import ausführen,
wird `count_words.py` *ebenfalls ausgeführt(!)*, d.h. die weiteren Module
werden importiert und die Funktion wird definiert, als hätten wir das
Programm direkt aufgerufen. Allerdings hat es diesmal nicht den Namen
`__main__`. Folglich wird der untere Teil des Programms ignoriert, den
wir nur brauchen, wenn wir das Modul direkt laufen lassen.

Natürlich ist es nicht nötig, dass ein Modul auch als eigenständiges
Programm ausführbar ist. Sie können eine `.py`-Datei erstellen, die
ausschließlich aus Funktions- und anderen Definitionen besteht und daraus
beliebig importieren. Bspw. kann man eine Datei `helper.py` anlegen, in
der man alle möglichen, nützlichen Funktionen sammelt, die man dann in
verschiedenen Projekten nutzen kann.

Mehr Infos zum Thema Modularisierung gibt es z.B.
[hier](https://www.python-course.eu/python3_modules_and_modular_programming.php).
