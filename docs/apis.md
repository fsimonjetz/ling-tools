# APIs <a title="Sourcecode auf bitbucket.org ansehen" href="https://bitbucket.org/fsimonjetz/ling-tools/raw/master/docs/apis.md"><img src="../img/bitbucket-icon.png" alt="Bitbucket icon"/></a>

## Grundidee

Daten, wie z.B. Wortinformationen aus dem DWDS, Wikipedia-Artikel uvm., können
per HTTP-Anfrage von Online-Schnittstellen abgefragt werden. Die Antwort
kommt in der Regel im JSON-Format.

## Am Beispiel DWDS

Das DWDS bietet eine [gut dokumentierte API](https://www.dwds.de/d/api).
Abfragen bestehen aus einer Basis-URL wie bspw.
`https://www.dwds.de/api/frequency` zur Abfrage von Worthäufigkeiten, die um
Suchparameter ergänzt wird. Die Suchparameter stehen im Format `key=value`
und sind durch ein Fragezeichen `?` von der Basis-URL getrennt. Falls es
mehrere Parameter gibt, sind diese wiederum durch `&` getrennt.

### Beispiele für DWDS-Abfragen

| URL                                             | Ergebnis                        |
|-------------------------------------------------| --------------------------------|
| `https://www.dwds.de/api/wb/snippet/?q=Haus`    | Häufigkeit für das Lemma "Haus" |
| `https://www.dwds.de/api/ipa/?q=Haus`           | Aussprache in IPA               |
| `https://www.dwds.de/r/?q=Haus&view=csv`        | Korpusbelege in CSV-Format      |
|`https://www.dwds.de/r?q=Hund&corpus=dtak&view=json`| Abfrage für das Lemma "Hund" im Deutschen Textarchiv (1598-1913)|


(Die letzte Abfrage liefert als ersten Treffer interessanterweise ein Kochbuch...)

Eine Liste der Abrufbaren Korpora finden Sie [hier](https://www.dwds.de/r).
Die ID für das jeweilige Subkorpus, die Sie in der Abfrage eingeben müssen,
können Sie dabei ermitteln, indem Sie sich den Link zum jeweiligen Subkorpus
anschauen. Korpora, die nicht frei durchsucht werden können, sind auch nicht
per IPA abrufbar. Wenn man es trotzdem versucht, merkt man recht schnell,
dass etwas schiefgelaufen ist, da die Abfrage des JSON einen Fehler verursacht.
Ein Blick auf das `.text`-Attribut der Abfrage verrät uns, dass wir trotz
der `view=json`-Option HTML erhalten haben. In Jupyter Notebooks kann der
HTML-Sourcecode direkt angezeigt werden, sodass wir auf einen Blick sehen,
dass es sich um eine Fehlerseite handelt.

![HTML in Jupyter Notebook](img/html_jupyter.png)


In Python verwendet man das `requests`-Modul, um APIs anzusteuern, z.B. so:

```python
import requests

abfrage = requests.get("https://www.dwds.de/api/wb/snippet/?q=Haus")
```

Das Ergebnis ist, wenn alles gut gelaufen ist, bspw. in `abfrage.json()` verfügbar. Falls nicht, hilft ein Blick auf
`.content` oder `.text`; ggf. liegen die Daten in einem anderen
Format vor, oder bei der Abfrage hat etwas nicht funktioniert.

## Limits
Die wenigsten APIs sind in vollem Umfang frei verfügbar. Zumeist gibt es
Beschränkungen der Abfragen, die von einer bestimmten IP kommen. Es kann also
passieren, dass Sie (vorübergehend) gesperrt werden, wenn Sie bspw. versuchen,
per `for`-Loop mehrere Abfragen in kurzer Zeit zu machen.

Eine Möglichkeit, das zu umgehen, ist das Setzen einer Verzögerung zwischen
Abfragen. In Python geht das am einfachsten per `time`-Modul:

```python
from time import sleep

sleep(20) # 20 Sekunden warten
```

Für andere APIs ist eine Registrierung erforderlich (so auch für Twitter).
Üblicherweise bekommen Sie dann einen Secret Key, den Sie bei der Abfrage
mit angeben müssen, bspw. so:

```python
abfrage = requests.get(url, headers={"SECRET_KEY":"Key_vom_Service"})
```



## Ein paar interessante APIs

- Wikipedia-Artikel
    - Beispiel-Abfrage der Zusammenfassung für den Artikel "Linguistics": `https://en.wikipedia.org/w/api.php?format=json&action=query&prop=extracts&exintro&explaintext&redirects=1&titles=Linguistics`
    - Es gibt aber auch eine [Python-Bibliothek](https://pypi.org/project/Wikipedia-API/), mit der das ganze viel einfacher gehen sollte (ohne Gewähr)
- Witze-APIs
    - [Allgemein](https://github.com/15Dkatz/official_joke_api)
    - [Chuck Norris](https://api.chucknorris.io/)
- Für Liebhaber
    - [Pokemon](https://pokeapi.co/docs/v2)
    - [Star Wars](https://swapi.dev/)
- Für LinguistInnen
    - [Fun translations](https://funtranslations.com/)
