# Bots <a title="Sourcecode auf bitbucket.org ansehen" href="https://bitbucket.org/fsimonjetz/ling-tools/raw/master/docs/bots.md"><img src="../img/bitbucket-icon.png" alt="Bitbucket icon"/></a>

## Vorlage für einen Chatbot

### Credits
Der Code ist eine Modifikation [dieser Vorlage](https://github.com/sahil-rajput/Candice-YourPersonalChatBot).

### Benutzung

Vorab: **Sie müssen nicht alle Details der Anwendung verstehen**, also keine
Panik, falls Sie mit einigen Punkten nichts anfangen können. Wichtig ist
letztlich nur die Bot-Funktion in `my_bot.py`, die weiter unten beschrieben
ist und von Ihnen verändert werden kann.

Unter [src/examples/chat_app](https://bitbucket.org/fsimonjetz/ling-tools/src/master/src/examples/chat_app/) liegt eine einfache
Chat-Anwendung bereit. Bevor Sie das Programm starten, müssen Sie vermutlich
[Flask](https://flask.palletsprojects.com/en/1.1.x/)
installieren: `pip install Flask`.  Flask ist eines von mehreren
Python-Frameworks, mit denen sich Webseiten generieren lassen.
Der Vorteil für uns ist, das wir sehr einfach einen Python-basierten Bot
integrieren können, der per HTML-Interface angesprochen werden kann. Wenn Sie
das Modul `app.py`bspw. aus der Kommandozeile mit dem Befehl `python app.py`
starten, sollte ein lokaler Server eingerichtet werden. Gehen Sie auf den
angezeigten Link (z.B. `http://127.0.0.1:5000/`), sollten Sie ein Interface
sehen, in dem Sie mit dem "YouSaidBot" chatten können, der nichts weiter tut,
als Dinge zu wiederholen, die im Verlauf des Chats gesagt wurden.

![Bot Interface](img/bot.png)


### Programmstruktur

```
chat_app
├── app.py
├── my_bot.py
├── static
│   ├── css
│   │   └── style.css
│   └── js
│       └── chat.js
└── templates
    └── home.html
```

Der Kern der Anwendung ist die Flask App, die wir in `app.py` definieren.
Diese baut zunächst mithilfe der Funktion `render_template()` und
der Datei `templates/home.html` die Webseite. `home.html` beinhaltet dabei kein
eigenständiges HTML, sondern ein *Template* in
[Jinja-Format](https://jinja.palletsprojects.com/en/2.11.x/), d.h. bestimmte
Elemente der Seite sind variabel und werden durch das Python-Script gefüllt.
Diese Elemente sind an doppelten geschweiften Klammern `{{ }}` erkennbar und
werden beim Aufruf von `render_template()` als
Keyword-Argumente vergeben. Sie können diese Elemente beliebig modifizieren,
solange die Namen im Template und im Python-Code übereinstimmen.

![Bot Structure](img/bot_struc.png)

Das Aussehen der Elemente der Seite (Abstände, Schriften, Rahmen usw.) wird
durch die Datei `static/css/style.css` gesteuert, die oben im HTML-Header
verlinkt ist. Auch hier können Sie beliebig Änderungen vornehmen.
Aufgrund von Caching kann es jedoch sein, dass die Änderungen nicht direkt
sichtbar werden; hier muss man ggf. den Browser neu starten oder den Cache
leeren. Generell ist es daher eine gute Idee, zum Testen die Seite als ein
Inkognito- oder "privates" Fenster zu öffnen; das hat einen eigenen Cache, der
geleert wird, sobald das Fenster geschlossen wird.

Die Funktionalität der Anwendung wird hauptsächlich per JavaScript
(inkl. jQuery-Bibliothek) gesteuert. Werfen Sie mal einen Blick
in `static/js/chat.js` und versuchen Sie, den Aufbau des Programms
zu verstehen: obwohl die Syntax sich stark unterscheidet, wird Ihnen vieles aus
Python vertraut vorkommen. Entscheidend ist die Kommunikation mit der
Python-App per `GET`-Request jedes mal, wenn der Nutzer eine
Eingabe macht.


### Chatbot

Der Bot selbst wird in `my_bot.py` definiert und in `app.py` importiert.
Ab Zeile 35 in `app.py` wird dem Bot bei jeder neuen Nutzernachricht der
aktualisierte Chatverlauf übergeben und die Antwort des Bots an das
Web-Interface gesendet. Der Beispiel-Bot wählt lediglich eine zufällige
Nachricht aus dem Chatverlauf und postet diese mit einer leichten
Verzögerung (um den Anschein zu erwecken, dass er überlegt). Daraus etwas
sinnvolles zu machen, liegt an Ihnen!

Es gibt einen "magischen" Befehl, `!clear`, der bei Eingabe den Chatverlauf
in `app.py` löscht und in `chat.js` die Anzeige zurücksetzt. Im Zweifelsfall
sollten Sie keine Änderungen an diesen Programmteilen machen.


### Lernfähige Bots mit Chatterbot

Statt den Bot von Hand zu programmieren, kann man auch das
[Chatterbot-Modul](https://chatterbot.readthedocs.io/en/stable/#)
benutzen, das speziell für diesen Zweck entwickelt wurde und **lernfähig** ist:
Der Bot wird erst mithilfe einer (möglichst großen) Menge von Beispielfragen
und -antworten trainiert und kann anschließend auch Variationen dieser
Trainingsabfragen verstehen. Um Ihre Programmierkenntnisse zu üben/erweitern,
ist diese Herangehensweise aber m.E. weniger interessant, weil Sie nur noch
Beispielabfragen sammeln und der Bot den ganzen Rest erledigt.
