# Repository zum Kurs "Entwicklung linguistischer Tools" (WS 20/21)

Das Repository enthält Code und Dokumentation für den o.g. Kurs.
Die Markdown-Dateien unter [docs/](docs/) werden mit [mkdocs](https://mkdocs.readthedocs.io/) gerendert und unter
[https://ling-tools.readthedocs.io](https://ling-tools.readthedocs.io/) abgelegt.
Unter [src/examples](src/examples) stehen Code-Beispiele zur Verfügung.