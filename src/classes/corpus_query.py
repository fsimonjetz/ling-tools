from corpus_reader import *
from nltk import ngrams


class Query():
    """Query a corpus for simple linear patterns like 'the N is/was that/IN'.
       Patterns are encoded as lists of dictionaries that work as constraints.
       For example, the above pattern could be translated as

       [{"text":"the"}, {"pos":"NN"}, {"lemma":"be"}, {"text":"that", "pos":"IN"}]
       """

    def __init__(self, q):
        self.query = q

    def match(self, corpus):
        """Given a corpus, get all matches for the query

        corpus: a list of Document objects"""

        n = len(self.query)
        matches = list()

        for doc in corpus:
            for sub_sequence in ngrams(doc.tokens, n):
                if self._match(sub_sequence):
                    matches.append(sub_sequence)

        return matches

    def _match(self, tok_sequence):
        # return True if all tokens in tok_sequence fulfill the
        # query's requirements

        for q, tok in zip(self.query, tok_sequence):
            for k, v in q.items():
                if getattr(tok, k, None) != v:
                    return False

        return True

    def pprint(self):
        # Pretty output
        output = list()

        for t in self.query:
            tok_string = "["
            tok_string += " & ".join(f"{k}='{v}'" for k, v in t.items())
            output.append(tok_string + "]")

        return " ".join(output)


if __name__ == '__main__':

    # Define patterns
    patterns = [[{"text": "the"}, {"pos": "NN"}, {"text": "that", "pos": "IN"}],
                [{"text": "the"}, {"pos": "NN"}, {"lemma": "be"}, {"text": "that", "pos": "IN"}]]

    corpus_path = "sn_corpus/en"
    documents = read_corpus(corpus_path)

    for i, p in enumerate(patterns, start=1):
        query = Query(p)
        result = query.match(documents)

        print("+" * 80)
        print(f"Query {i}:", query.pprint())
        print()
        print(f"Found {len(result)} matches:")

        for r in result[:5]:
            print(r)

        print("...\n")
