## Korpus-Abfragetool

- Wir wollen in einem Korpus abstrakte Nomen wie *fact*, *plan*, *problem*,
  *idea* u.a. suchen, die bevorzugt in bestimmten syntaktischen Konstruktionen
  vorkommen:
    - \<s\> *this*/*that* N
    - *the* N *that*
    - *the* N *if*/*whether*
    - *the* N *to* V
    - *the* N *is that*
    - *the* N *is if*/*whether*
    - *the* N *is to* V

### Vorgehen
- Das Korpus muss in einem geeigneten Format eingelesen werden
- Wir müssen uns überlegen, wie wir die Abfragen realisieren

### Korpus
- Bereits vorverarbeitete Redebeiträge aus dem Europäischen Parlament
- JSON-Format
- Auch [hier](https://github.com/ajroussel/shell-nouns-data) verfügbar

### Lösungshinweise
- [corpus_reader.py](corpus_reader.py) enthält zwei Klassen: `Document` und
  `Token`. Die Funktion `read_corpus()` liest die JSON-Dateien des angegebenen
  Ordners ein und erzeugt daraus einzelne `Document`-Objekte. Zentrales Element
  der Dokumente ist die Liste von `Token`-Objekten im Attribut `.tokens`. Dies
  ermöglicht eine einfache Abfrage von Token-Eigenschaften, entweder per
  Punkt-Notation (z.B. `token.lemma`) oder per `getattr(token, "lemma")`.
  Methoden mit doppelten Underscores sind magische Methoden, die die Nutzung
  von built-in Funktionen wie `str()`, `len()` u.a. steuern.
- [corpus_query.py](corpus_query.py) enthält die `Query`-Klasse, die ein Pattern
  als Liste von Dictionarys entgegennimmt und Dokumente danach durchsucht.
  Gegeben ein Pattern der Länge `n` extrahiert der Matching-Algorithmus
  Token-Sequenzen passender Länge aus dem Korpus und gleicht diese mit den
  Vorgaben aus dem Pattern ab.

### Warum das cool ist (manchmal)
- Mithilfe von Klassen kann man Funktionen, die konzeptuell zusammengehören,
  auch im Code bündeln. Die Funktionen können immer auf alle Attribute des
  Objekts zugreifen, was den Code bei komplexeren Datenstrukturen konziser
  machen kann.
- Der (zunächst) höhere Programmieraufwand lohnt sich in Aufgaben wie in diesem
  Beispiel, denn:
    - Die `Document`- und `Token`-Klassen haben ein recht generisches Format:
      Wenn wir später mit einem anderen Korpus arbeiten möchten, können wir
      relativ einfach eine alternative Einlese-Methode schreiben und danach mit
      denselben Objekten weiterarbeiten.
    - Der Matching-Algorithmus abstrahiert die Patterns, sodass wir nun alle
      möglichen Constraints abgleichen können, ohne bei jedem Pattern einen neuen
      `for`-Loop überlegen zu müssen. Das hat sich spätestens nach dem zweiten
      oder dritten Pattern gelohnt!

### Erweiterungen
- Der Code ist nur ein Prototyp, den man beliebig erweitern könnte, z.B.:
    - Oben bereits angesprochene alternative Einlese-Methoden für zusätzliche
      Datenformate in der `Document`-Klasse.
    - Die Tokens haben je ein `head`- und `dep`-Attribut, mit dem man die
      syntaktische Struktur des jeweiligen Satzes rekonstruieren kann - es wäre
      eigentlich wünschenswert, wenn `tok.head` tatsächlich das entsprechende
      `Token` zurückgibt, und nicht nur den Index.
    - Es gibt in jedem Satz ein "Root"-Token, was keinen Kopf hat. Da es sich
      hierbei um ein besonderes Token handelt, könnte man mittels Vererbung
      eine Subklasse `RootToken` definieren, die auf `Token` basiert.
    - Statt Strings abzugleichen, könnte man in den Patterns mit regulären
      Ausdrücken arbeiten. Das wäre eine mächtige Erweiterung und wesentlich
      flexibler.
    - Momentan sind keine optionalen Elemente in den Patterns möglich. Wenn wir
      diese nur selten brauchen, können wir ein Pattern der Form "A(B)C" vorerst
      umschreiben in zwei Patterns "AC" und "ABC", aber langfristig möchte man
      optionale Elemente eher direkt implementieren, was auch eine interessante
      Herausforderung ist.
