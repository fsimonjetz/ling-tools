import json
import os

class Token():
    """Represents a token"""

    def __init__(self, tok_data):
        # set attributes
        self.index = int(tok_data["index"])
        self.lemma = tok_data["lemma"]
        self.morph = tok_data["morph"]
        self.head = int(tok_data["mother"])
        self.pos = tok_data["pos"]
        self.dep = tok_data["rel"]
        self.text = tok_data["text"]

    def __str__(self):
        # gets called by built-in str() function and print()
        return self.text

    def __repr__(self):
        # for built-in repr() function
        return repr(self.text)

    def __len__(self):
        # gets called by built-in len() function
        return len(self.text)

    def is_noun(self):
        # An example for a custom method
        return self.pos.startswith("N")


class Document():
    """Represents a document"""

    def __init__(self, data):
        self.data = data
        self.name = data['name']
        self.lang = data['lang']
        self.tokens = [Token(d) for d in data["tokens"]]
        self.sentences = self._get_sentences()

    def _get_sentences(self):
        """It is good practice to name methods for internal use with a preceding underscore '_' """
        sents = list()

        for start, end in self.data["sentences"]:
            sents.append(self.tokens[start:end])

        return sents


def read_corpus(corpus_path):
    """Read a corpus in json format"""

    documents = list()

    # turn all input files into Document objects
    for fp in os.listdir(corpus_path):
        if fp.endswith(".json"):
            with open(os.path.join(corpus_path, fp), mode="r", encoding="utf-8") as jsonfile:
                data = json.load(jsonfile)

            doc = Document(data)
            documents.append(doc)

    return documents


if __name__ == "__main__":
    corpus_path = "sn_corpus/en"
    corpus = read_corpus(corpus_path)

