
function getSelectedToken() {

  var selection = window.getSelection();

  if ((selection.anchorNode) && (selection.anchorOffset < selection.extentOffset)) {

    var node = selection.anchorNode.parentNode;

    if (node.classList.contains("token")) {
      return node.id;
    }
  }
}


function addAnnotation(tag) {
  var tok_id = getSelectedToken();

  if (tok_id) {
    // send the new annotation to the server and display the complete annotation list
    $.get("/save", { tok_id: tok_id, tag:tag }).done(showAnno);

    // update the css class of the token
    document.getElementById(tok_id).classList.add(tag);
  }
}


function showAnno(data) {
  document.getElementById("result").innerHTML = data;
}
