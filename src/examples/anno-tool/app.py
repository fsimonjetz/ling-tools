'''
Simple Annotation Tool
'''

from flask import Flask, render_template, request, url_for
from collections import defaultdict
import json

app = Flask(__name__)


tokens = [(1, 'Das'), (2, 'ist'), (3, 'ein'), (4, 'Beispiel'), (5, ','),
          (6, 'wie'), (7, 'man'), (8, 'im'), (9, 'Browser'), (10, 'auf'),
          (11, 'markierten'), (12, 'Text'), (13, 'zugreifen'), (14, 'und'),
          (15, 'Elemente'), (16, 'modifizieren'), (17, 'kann'), (18, '.')]

# info about defaultdict: https://ling-tools.readthedocs.io/en/latest/tips/#collections
annotations = defaultdict(set)

@app.route("/")
def home():
    return render_template("index.html", tokenlist=tokens)


@app.route("/save")
def save():
    '''Save new annotation'''

    # the request is passed by the function in addAnnotation() index.js
    tok_id = request.args.get('tok_id')
    tag = request.args.get('tag')

    # print data to the console for debugging
    # print("ID:", tok_id)
    # print("TAG:", tag)

    # add annotation to the database
    annotations[tok_id].add(tag)

    # return the dictionary as JSON object
    # the sets need to be converted into lists because the json module
    # can't handle sets; cf. https://ling-tools.readthedocs.io/en/latest/tips/#json
    return json.dumps({k:list(v) for k,v in annotations.items()})



# run app: start with `python app.py` and open the link in your browser
if __name__ == "__main__":
    app.run(debug=True)
