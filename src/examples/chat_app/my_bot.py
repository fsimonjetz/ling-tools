import random
from time import sleep

def youSaidBot(chat_hist):
    '''A lazy bot that just repeats stuff from the chat history'''

    # random delay between 0 and 2 seconds
    sleep(random.randint(0,2))

    # pick random message from history
    t = random.choice(chat_hist)

    # reply
    return f"Someone here said '{t}'"
