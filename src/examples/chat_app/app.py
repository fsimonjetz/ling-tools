'''
Chatbot inspired by and based on
https://github.com/sahil-rajput/Candice-YourPersonalChatBot
'''

from flask import Flask, render_template, request, url_for, redirect
from my_bot import youSaidBot

app = Flask(__name__)

# initialize chat history
chat_history = list()

first_msg = "Greetings!"

@app.route("/")
def home():
    return render_template("home.html",
                            welcome_msg=first_msg,
                            botname="YouSaidBot",
                            botinfo="A simple bot that repeats random stuff you said")

@app.route("/get")
def get_bot_response():

    # Get user input
    user_text = request.args.get('msg')

    # if user issues magic command, clear history
    if user_text == "!clear":
        chat_history.clear()
        return first_msg

    else:
        chat_history.append(user_text)

        ####################################
        #            Be creative           #
        ####################################

        bot_reply = youSaidBot(chat_history)
        chat_history.append(bot_reply)

        return bot_reply

        ####################################
        #        Stop being creative       #
        ####################################

# run app: start with `python app.py` and open the link in your browser
if __name__ == "__main__":
    app.run(debug=True)
