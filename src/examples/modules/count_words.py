from collections import Counter
from pprint import pprint
import sys

def word_stats(text):

    words = Counter(text.split())

    return words
    

if __name__ == "__main__":
    
    arguments = sys.argv
    
    with open(arguments[1], mode="r", encoding="utf-8") as infile:
        text = infile.read()
    
    pprint(word_stats(text))

